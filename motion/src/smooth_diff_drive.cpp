#include "motion/smooth_diff_drive.h"

using namespace motion;

Smooth_Diff_Drive::Smooth_Diff_Drive() {
    init();
}

Smooth_Diff_Drive::~Smooth_Diff_Drive(){

}

bool Smooth_Diff_Drive::init(){
    // Initialize the odometry position
    odom_.pose.pose.position.x = 0.0;
    odom_.pose.pose.position.y = 0.0;
    odom_.pose.pose.position.z = 0.0;
    yaw_ = 0.0;
    odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_);

    // Intialize the odometry twist
    r_ = 0.033;
    l_ = 0.16;
    v_r_ = 0.0;
    v_l_ = 0.0;
    lin_limit_ = 10;
    odom_.twist.twist.linear.x = (v_r_ + v_l_) / 2;
    odom_.twist.twist.linear.y = 0.0;
    odom_.twist.twist.linear.z = 0.0;
    odom_.twist.twist.angular.x = 0.0;
    odom_.twist.twist.angular.y = 0.0;
    odom_.twist.twist.angular.z = 1 / l_ * (v_r_ - v_l_);


    nh_.param("odom_frame", odom_.header.frame_id, std::string("odom"));
    nh_.param("base_frame", odom_.child_frame_id, std::string("base_footprint"));

    double pcov[36] = { 0.1,   0,   0,   0,   0, 0,
                          0, 0.1,   0,   0,   0, 0,
                          0,   0, 1e6,   0,   0, 0,
                          0,   0,   0, 1e6,   0, 0,
                          0,   0,   0,   0, 1e6, 0,
                          0,   0,   0,   0,   0, 0.2};
    memcpy(&(odom_.pose.covariance),pcov,sizeof(double)*36);
    memcpy(&(odom_.twist.covariance),pcov,sizeof(double)*36);

    // initialize publishers
    odom_pub_         = nh_.advertise<nav_msgs::Odometry>("odom", 100);

    // initialize subscribers
    cmd_accel_sub_  = nh_.subscribe("cmd_accel", 100,  &Smooth_Diff_Drive::commandAccelCallback, this);

    prev_update_time_ = ros::Time::now();
    return true;
}

bool Smooth_Diff_Drive::update(){
    ros::Time time_now = ros::Time::now();
    ros::Duration step_time = time_now - prev_update_time_;
    prev_update_time_ = time_now;

    // odom
    updateOdometry(step_time);
    odom_.header.stamp = time_now;
    odom_pub_.publish(odom_);

    // tf
    geometry_msgs::TransformStamped odom_tf;
    updateTF(odom_tf);
    tf_broadcaster_.sendTransform(odom_tf);

    return true;
}

void Smooth_Diff_Drive::commandAccelCallback(const motion::DifferentialAccelerationConstPtr cmd_accel_msg){
    a_r_ = cmd_accel_msg->right_accel;
    a_l_ = cmd_accel_msg->left_accel;
}

bool Smooth_Diff_Drive::updateOdometry(ros::Duration diff_time){
    double dt = diff_time.toSec();

    v_r_ = limitOutput(v_r_ + dt * a_r_, lin_limit_);
    v_l_ = limitOutput(v_l_ + dt * a_l_, lin_limit_);

    // Use Euler integration for updating the odometry
    common_ = r_ / 2 * (v_r_ + v_l_);
    odom_.pose.pose.position.x = odom_.pose.pose.position.x + dt * common_ * std::cos(yaw_);
    odom_.pose.pose.position.y = odom_.pose.pose.position.y + dt * common_ * std::sin(yaw_);

    yaw_ = yaw_ + dt * r_ / l_ * (v_r_ - v_l_);
    odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_);

    // Update the odometry twist
    odom_.twist.twist.linear.x = (v_r_ + v_l_) / 2;
    odom_.twist.twist.angular.z = 1 / l_ * (v_r_ - v_l_);    

    return true;
}

double Smooth_Diff_Drive::limitOutput(double orig, double limit){
    return std::max(std::min(orig, limit), -limit); 
}

void Smooth_Diff_Drive::updateTF(geometry_msgs::TransformStamped& odom_tf){
    odom_tf.header = odom_.header;
    odom_tf.child_frame_id = odom_.child_frame_id;
    odom_tf.transform.translation.x = odom_.pose.pose.position.x;
    odom_tf.transform.translation.y = odom_.pose.pose.position.y;
    odom_tf.transform.translation.z = odom_.pose.pose.position.z;
    odom_tf.transform.rotation = odom_.pose.pose.orientation;
}

/*******************************************************************************
* Main function
*******************************************************************************/
int main(int argc, char* argv[])
{
  ros::init(argc, argv, "smooth_diff_drive_node");
  Smooth_Diff_Drive smooth_diff_drive;

  ros::Rate loop_rate(30);

  while (ros::ok())
  {
    smooth_diff_drive.update();
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
