#ifndef SMOOTH_DIFF_DRIVE_H_
#define SMOOTH_DIFF_DRIVE_H_

#include <ros/ros.h>
#include <ros/time.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TransformStamped.h>
#include <motion/DifferentialAcceleration.h>

#include <tf/tf.h>
#include <tf/transform_broadcaster.h>

namespace motion{


class Smooth_Diff_Drive
{
public:
    Smooth_Diff_Drive();
    ~Smooth_Diff_Drive();
    bool init();
    bool update();

private:
    // ROS NodeHandle
    ros::NodeHandle nh_;

    // ROS Topic Publishers
    ros::Publisher odom_pub_;

    // ROS Topic Subscribers
    ros::Subscriber cmd_accel_sub_;

    // ROS time for updates
    ros::Time prev_update_time_;

    // Variables
    double r_;  // radius of wheels
    double l_;  // wheel base (distance from left to right wheel)
    double v_r_;  // velocity of right wheel
    double v_l_;  // velocity of left wheel
    double common_; // common term used to simply expression
    double yaw_;  // yaw of vehicle (theta)
    double lin_limit_;  // limit value of linear velocity
    double ang_limit_;  // limit value of angular velocity

    double a_r_;  // input acceleration of right wheel
    double a_l_;  // input acceleration of left wheel
    nav_msgs::Odometry odom_;
    tf::TransformBroadcaster tf_broadcaster_;

    // Function prototypes
    // void commandAccelCallback(const geometry_msgs::TwistConstPtr cmd_accel_msg);
    void commandAccelCallback(const motion::DifferentialAccelerationConstPtr cmd_accel_msg);
    bool updateOdometry(ros::Duration diff_time);
    void updateTF(geometry_msgs::TransformStamped& odom_tf);
    double limitOutput(double, double);
};

}


#endif // SMOOTH_DIFF_DRIVE_H_
