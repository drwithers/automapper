#include "vel_control/unicycle.h"

using namespace dynamic_models;

Unicycle::Unicycle() {
    init();
}

Unicycle::~Unicycle(){

}

bool Unicycle::init(){
    // Initialize state variables
    linear_velocity_ = angular_velocity_ = 0.0;

    // Initialize desired velocities
    linear_velocity_desired_ = angular_velocity_desired_ = 0.0;


    r_ = 0.033;
    l_ = 0.16;
    v_r_ = 0.0;
    v_l_ = 0.0;

    // Initialize feedback matrix
    k11_ = 3.162; // Feedback used to calculate linear acceleration input
    k12_ = 0.0;
    k21_ = 0.0;    // Feedback used to calculate angular acceleration input
    k22_ = 14.142;

    // Initialize the odometry position
    odom_.pose.pose.position.x = 0.0;
    odom_.pose.pose.position.y = 0.0;
    odom_.pose.pose.position.z = 0.0;
    yaw_ = 0.0;
    odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_);

    // Intialize the odometry twist
    linear_velocity_ = 0.0;
    angular_velocity_ = 0.0;
    odom_.twist.twist.linear.x = linear_velocity_;
    odom_.twist.twist.linear.y = 0.0;
    odom_.twist.twist.linear.z = 0.0;
    odom_.twist.twist.angular.x = 0.0;
    odom_.twist.twist.angular.y = 0.0;
    odom_.twist.twist.angular.z = angular_velocity_;


    nh_.param("odom_frame", odom_.header.frame_id, std::string("odom"));
    nh_.param("base_frame", odom_.child_frame_id, std::string("base_footprint"));

    double pcov[36] = { 0.1,   0,   0,   0,   0, 0,
                          0, 0.1,   0,   0,   0, 0,
                          0,   0, 1e6,   0,   0, 0,
                          0,   0,   0, 1e6,   0, 0,
                          0,   0,   0,   0, 1e6, 0,
                          0,   0,   0,   0,   0, 0.2};
    memcpy(&(odom_.pose.covariance),pcov,sizeof(double)*36);
    memcpy(&(odom_.twist.covariance),pcov,sizeof(double)*36);

    // initialize publishers
    odom_pub_         = nh_.advertise<nav_msgs::Odometry>("odom", 100);

    // initialize subscribers
    cmd_vel_sub_  = nh_.subscribe("cmd_vel", 100,  &Unicycle::commandVelocityCallback, this);

    prev_update_time_ = ros::Time::now();
    return true;
}

bool Unicycle::update(){
    ros::Time time_now = ros::Time::now();
    ros::Duration step_time = time_now - prev_update_time_;
    prev_update_time_ = time_now;

    // odom
    updateOdometry(step_time);
    odom_.header.stamp = time_now;
    odom_pub_.publish(odom_);

    // tf
    geometry_msgs::TransformStamped odom_tf;
    updateTF(odom_tf);
    tf_broadcaster_.sendTransform(odom_tf);

    return true;
}

void Unicycle::commandVelocityCallback(const geometry_msgs::TwistConstPtr cmd_vel_msg){
    linear_velocity_desired_ = cmd_vel_msg->linear.x;
    angular_velocity_desired_ = cmd_vel_msg->angular.z;
}

bool Unicycle::updateOdometry(ros::Duration diff_time){
    double dt = diff_time.toSec();

    double u_v = -(k11_*(linear_velocity_-linear_velocity_desired_) + k12_*(angular_velocity_-angular_velocity_desired_));
    double u_w = -(k21_*(linear_velocity_-linear_velocity_desired_) + k22_*(angular_velocity_-angular_velocity_desired_));

    a_r_ = (2*u_v + l_*u_w) / 2;
    a_l_ = (2*u_v - l_*u_w) / 2;

    v_r_ = v_r_ + dt * a_r_;
    v_l_ = v_l_ + dt * a_l_;

    // Use Euler integration for updating the odometry
    common_ = r_ / 2 * (v_r_ + v_l_);
    odom_.pose.pose.position.x = odom_.pose.pose.position.x + dt * common_ * std::cos(yaw_);
    odom_.pose.pose.position.y = odom_.pose.pose.position.y + dt * common_ * std::sin(yaw_);

    yaw_ = yaw_ + dt * r_ / l_ * (v_r_ - v_l_);
    odom_.pose.pose.orientation = tf::createQuaternionMsgFromYaw(yaw_);

    // Update the odometry twist
    linear_velocity_ = (v_r_ + v_l_) / 2;
    angular_velocity_  = 1 / l_ * (v_r_ - v_l_); 
    odom_.twist.twist.linear.x = linear_velocity_; 
    odom_.twist.twist.angular.z = angular_velocity_;    

    return true;
}

void Unicycle::updateTF(geometry_msgs::TransformStamped& odom_tf){
    odom_tf.header = odom_.header;
    odom_tf.child_frame_id = odom_.child_frame_id;
    odom_tf.transform.translation.x = odom_.pose.pose.position.x;
    odom_tf.transform.translation.y = odom_.pose.pose.position.y;
    odom_tf.transform.translation.z = odom_.pose.pose.position.z;
    odom_tf.transform.rotation = odom_.pose.pose.orientation;
}

/*******************************************************************************
* Main function
*******************************************************************************/
int main(int argc, char* argv[])
{
  ros::init(argc, argv, "unicycle_dynamics_node");
  Unicycle unicycle;

  ros::Rate loop_rate(30);

  while (ros::ok())
  {
    unicycle.update();
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
